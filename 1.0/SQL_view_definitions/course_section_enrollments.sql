CREATE SCHEMA IF NOT EXISTS f_report_card;

DROP MATERIALIZED VIEW IF EXISTS f_report_card.course_section_enrollments;

CREATE MATERIALIZED VIEW f_report_card.course_section_enrollments
TABLESPACE pg_default
AS WITH person_min AS (
         SELECT DISTINCT p_1.person_id,
            p_1.sex,
            p_1.edu_level_parental,
            p_1.ethnicity,
            p_1.us_residency
           FROM entity.person p_1
        ), course_section_min AS (
         SELECT DISTINCT cs_1.course_section_id,
            cs_1.course_offering_id
           FROM entity.course_section cs_1
        ), course_offering_min AS (
         SELECT DISTINCT co_1.course_offering_id,
            co_1.academic_term_id,
            co_1.course_subj,
            co_1.course_no,
            co_1.available_credits,
            co_1.title
           FROM entity.course_offering co_1
        ), academic_term_min AS (
         SELECT DISTINCT act_1.academic_term_id,
            act_1.name AS term_name,
            act_1.term_type,
            act_1.term_begin_date
           FROM entity.academic_term act_1
        )
 SELECT DISTINCT co.title AS course_title,
    co.course_subj AS course_subject,
    co.course_no AS course_number,
    co.course_subj::text || co.course_no::text AS course_code,
    act.term_name,
    act.term_type,
    act.term_begin_date,
    p.person_id,
        CASE
            WHEN p.sex::text = 'Female'::text THEN true
            ELSE false
        END AS is_female,
        CASE
            WHEN p.edu_level_parental::text = ANY (ARRAY['AS'::character varying, 'BA'::character varying, 'PB'::character varying, 'MD'::character varying, 'PM'::character varying, 'DO'::character varying, 'PD'::character varying, 'AE'::character varying, 'SC'::character varying, ''::character varying]::text[]) THEN false
            ELSE true
        END AS is_first_time_hed,
        CASE
            WHEN p.ethnicity::text = ANY (ARRAY['White'::character varying, 'Asian'::character varying, ''::character varying]::text[]) THEN false
            ELSE true
        END AS is_urm,
        CASE
            WHEN p.us_residency::text = ANY (ARRAY['USCitizen'::character varying, ''::character varying]::text[]) THEN false
            ELSE true
        END AS is_international
   FROM entity.course_section_enrollment cse
     JOIN person_min p ON cse.person_id = p.person_id
     LEFT JOIN course_section_min cs ON cs.course_section_id = cse.section_id
     LEFT JOIN course_offering_min co ON co.course_offering_id = cs.course_offering_id
     LEFT JOIN academic_term_min act ON act.academic_term_id = co.academic_term_id
  WHERE cse.role::text = 'Student'::text AND cse.role_status::text = 'Enrolled'::text
WITH DATA;