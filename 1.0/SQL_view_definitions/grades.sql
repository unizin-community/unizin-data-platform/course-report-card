CREATE SCHEMA IF NOT EXISTS f_report_card;

DROP MATERIALIZED VIEW IF EXISTS f_report_card.grades;

CREATE MATERIALIZED VIEW f_report_card.grades
TABLESPACE pg_default
AS 

with course_section_enrollment_min as (
    select distinct cse.section_id, cse.role_status, cse.person_id
    from entity.course_section_enrollment cse
),
    course_section_min as (
    select distinct cs.course_section_id, cs.course_offering_id
    from entity.course_section cs
),
    course_offering_min as (
    select distinct co.course_offering_id, co.academic_term_id, co.course_subj, co.course_no, co.available_credits
    from entity.course_offering as co
),
    academic_term_min as (
    select distinct act.academic_term_id, act."name" as "term_name", act.term_type, act.term_begin_date
    from entity.academic_term as act
)
    
    SELECT distinct act.term_name,
     act.term_type,
     act.term_begin_date,
     co.course_subj as "course_subject",
     co.course_no as "course_number",
     co.course_subj || co.course_no AS course_code,
     co.available_credits,
    case
        when cg.final_score >= 97 then 'A+'
        when cg.final_score >= 93 then 'A'
        when cg.final_score >= 90 then 'A-'
        when cg.final_score >= 87 then 'B+'
        when cg.final_score >= 83 then 'B'
        when cg.final_score >= 80 then 'B-'
        when cg.final_score >= 77 then 'C+'
        when cg.final_score >= 73 then 'C'
        when cg.final_score >= 70 then 'C-'
        when cg.final_score >= 67 then 'D+'
        when cg.final_score >= 63 then 'D'
        when cg.final_score >= 60 then 'D-'
        else 'F/I/W'
    end as "grade",
    case
        when cg.final_score >= 97 then 4.3
        when cg.final_score >= 93 then 4.0
        when cg.final_score >= 90 then 3.67
        when cg.final_score >= 87 then 3.33
        when cg.final_score >= 83 then 3.0
        when cg.final_score >= 80 then 2.67
        when cg.final_score >= 77 then 2.33
        when cg.final_score >= 73 then 2.0
        when cg.final_score >= 70 then 1.67
        when cg.final_score >= 67 then 1.33
        when cg.final_score >= 63 then 1.0
        when cg.final_score >= 60 then 0.67
        else 0.0
    end as "grade_points_per_credit",   
     cg.person_id
    FROM entity.course_grade cg
      JOIN course_section_enrollment_min as cse on cg.course_section_id = cse.section_id and cg.person_id = cse.person_id
      LEFT JOIN course_section_min cs ON cse.section_id = cs.course_section_id
      LEFT JOIN course_offering_min co ON cs.course_offering_id = co.course_offering_id
      LEFT JOIN academic_term_min act ON co.academic_term_id = act.academic_term_id
 where cg.final_score is not null and cg.status <> 'deleted';