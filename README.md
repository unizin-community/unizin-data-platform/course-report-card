# Course Report Card
This report analyzes courses across the following aspects of courses and enrolled students:
* Enrollment over time
* Demographic reference stats (URM, first time HED, etc.)
* Grades

## Project Owners
Heather Rypkema, Ph.D.  
Assistant Director  
Foundational Course Initiative  
Center for Research on Learning and Teaching  
University of Michigan  
hrypkema@umich.edu  

## Required UDP entities and Views
Note: 2.0 data are primarily used. 1.0 is supported, but 2.0 is the official version. If your context store is not on 2.0 yet, then use the 1.0 version. We encourage all institutions to upgrade to UDP 2.0

### UDP Entities
* entity.course_section_enrollment
* entity.person
* entity.course_section
* entity.course_offering
* entity.academic_session (Note: this is only needed in UDP 2.0)
* entity.academic_term
* entity.course_grade

### Custom Views and Schemas

#### 2.0
* um_course_report_card.enrollments
	* This materialized view focuses on enrollment at the course section enrollment, term level.
	* Custom boolean fields are calculated to capture demographic information (e.g. female, URM, first time HED, and international) for each enrollee
* um_course_report_card.course_grades
	* This materialized view focuses on grades input by the instructor and grade points associated with those grades
	* We do preliminary joins to filter on academic term and course offering to match the grain across the whole report
	* Person ID is pulled through for joining with enrollment

#### 1.0
* f_course_report.course_section_enrollments
* f_course_report.grades

Note: These views are functionally identical to the 2.0 views. In general, Unizin will develop for 2.0 first then retrofit back to 1.0 as needed.

## Running the Report

### Custom Views
Ensure that the above custom view(s) and schema(s) exist in your context store before running the report. Refer to the SQL_view_definitions folder if any of these need to be created.

### Parameters
There are two parameters scripted inline to run this report: `course_in_scope` and `term_in_scope`.
* `course_in_scope`: string value for the course upon which the analysis should run. Provide both the dept/subject (e.g. 'MATH') with the course number (e.g. '1440')
* `term_in_scope`: string value for the term upon which the analysis should run. Provide both the season (only 'Fall' or 'Spring') and year (e.g. '2019')
Default values have been provided as examples in the Rmd script.

Edit one or both of these parameters in RStudio prior to running as needed.

### DB Config
A config.yml file needs to be included for the DB connection. This varies based on the institution, and no connection info is hardcoded in the Rmd script for security. The format of the config.yml file should be the following:

```YAML
default:
  context_store:
    driver: 'Postgres'
    hostname: '<DB HOST NAME>'
    username: '<DB USERNAME>'
    password: '<DB PASSWORD>'
    port: 5432
    database: 'context_store'
```
If you are unsure of the correct values for these fields, please contact Unizin for support.

Note: Please ensure that the config.yml file is included in your .gitignore file so that sensitive credentials are not shared widely via the repo.


## Reporting Tools
R Studio is used for this analysis. The udp_course_reports.Rmd file can be uploaded into a workspace on [Unizin's R Studio server](https://rstudio.data-analytics.internal.cloud.unizin.org/)

Note: This requires access via Unizin's IDP. Please reach out to Unizin for access if needed.

## Release History
* 0.2 - April 9, 2020
    * grade distributions
    * initial 1.0 code support
* 0.1 - March 26, 2020
	* initial release of preliminary Rmd and README structures